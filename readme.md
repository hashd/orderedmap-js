# OrderedMap

Since JavaScript maps do not guarantee order during iteration, this is an class designed to overcome that con by maintaining more metadata and providing more abilities that a usual object/map.
