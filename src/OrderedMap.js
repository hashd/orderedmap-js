export class OrderedMap {
	constructor() {
		this._map = {}
		this._list = []
		this.length = 0
	}

	set(key, value = null) {
		let idx = this._list.indexOf(key)
		if (idx !== -1) {
			this._list.splice(idx, 1)
			this.length--
		}

		this._map[key] = value
		this._list.push(key)
		this.length++
	}

	get(key) {
		return this._map[key]
	}

	at(index) {
		if (index >= 0 && index < this._list.length) {
			let key = this._list[index]
			return { key, value: this._map[key] }
		}
		return undefined
	}

	remove(key) {
		let idx = this._list.indexOf(key),
			returnVal = undefined

		if (idx !== -1) {
			returnVal = this._map[key]
			this._list.splice(idx, 1)
			this.length--
			delete this._map[key]
		}

		return returnVal;
	}

	forEach(fun) {
		if (OrderedMap.isFunction(fun)) {
			this._list.forEach((k) => this.set(k, fun(k, this._map[k])))
		}
	}

	sort(fun) {
		if (OrderedMap.isFunction(fun)) {
			this._list.sort(fun);
		}
	}

	filter(fun) {
		if (OrderedMap.isFunction(fun)) {
			let filtered = []
			this._list.forEach(k => {
				if (fun(k, this._map[k])) {
					filtered.push(k)
					delete this._map[k]
				}
			})
			this._list = filtered
			this._length = filtered.length
		}
	}

	static isFunction(fun) {
		if (fun && {}.toString.call(fun) === '[object Function]') {
			return true
		}
		return false
	}
}